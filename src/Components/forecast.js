import React from "react";

const Forecast = props => {
  return (
    <div className="weather">
      {props.city && props.country && (
        <p>
          <b>Location: </b>
          {props.city}, {props.country}
        </p>
      )}
      {props.longitude && props.latitude && (
        <p>
          <b>Coordinate: </b>
          {props.longitude}, {props.latitude}
        </p>
      )}
      {props.temperature && (
        <p>
          <b>Temperature: </b>
          {props.temperature}
        </p>
      )}
      {props.tempmin && props.tempmax && (
        <p>
          <b>Temperature Range: </b>
          {props.tempmin}, {props.tempmax}
        </p>
      )}
      {props.humidity && (
        <p>
          <b>Humidity: </b>
          {props.humidity}
        </p>
      )}
      {props.pressure && (
        <p>
          <b>Presssure: </b>
          {props.pressure}
        </p>
      )}
      {props.icon && (
        <img
          src={`http://openweathermap.org/img/wn/${props.icon}@2x.png`}
          alt="Weather icon"
        />
      )}
      {props.description && (
        <p>
          <b>Condition: </b>
          {props.description}
        </p>
      )}
      {props.error && <p>{props.error}</p>}
    </div>
  );
};
export default Forecast;
