import React from "react";

const Heading = props => {
  return (
    <div>
      <h1>What's the Weather today?</h1>
      <h3>Find out the Weather conditon of your city.</h3>
    </div>
  );
};
export default Heading;
