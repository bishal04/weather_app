import React from "react";

const Form = props => {
  return (
    <div>
      <form onSubmit={props.loadWeather}>
        <input type="text" name="city" placeholder="Choose a city" />
        <input
          type="text"
          name="country"
          placeholder="Choose a country"
        ></input>
        <button type="submit">Get Weather</button>
      </form>
    </div>
  );
};
export default Form;
