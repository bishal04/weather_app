import React, { Component } from "react";
import "./App.css";
import Heading from "./Components/heading";
import Form from "./Components/form";
import Forecast from "./Components/forecast";

const api_key = "c6e97eca867494ad2ae170a2c28458ff";

class App extends Component {
  state = {
    temperature: "",
    tempmin:"",
    tempmax:"",
    city: "",
    longitude: "",
    latitude:"",
    country: "",
    humidity: "",
    pressure: "",
    icon: "",
    description: "",
    error: ""
  };

  getWeather = async e => {
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    console.log(city);
    console.log(country);
    e.preventDefault();

    const api_call = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&units=imperial&appid=${api_key}`
    );
    // For temperature in Fahrenheit use units=imperial
    // For temperature in Celsius use units=metric

    const response = await api_call.json();
    if (city && country) {
      this.setState({
        temperature: response.main.temp,
        tempmin: response.main.temp_min,
        tempmax: response.main.temp_max,
        city: response.name,
        longitude: response.coord.lon,
        latitude: response.coord.lat,
        country: response.sys.country,
        humidity: response.main.humidity,
        pressure: response.main.pressure,
        icon: response.weather[0].icon,
        description: response.weather[0].description,
        error: ""
      });
    } else {
      this.setState({
        error: "Please fill out the field....."
      });
    }
  };

  render() {
    return (
      <div className="container">
        <Heading />
        <div className="form">
          <Form loadWeather={this.getWeather} />
          <div className="weather">
            <Forecast
              temperature={this.state.temperature}
              tempmin={this.state.tempmin}
              tempmax={this.state.tempmax}
              city={this.state.city}
              longitude={this.state.longitude}
              latitude={this.state.latitude}
              country={this.state.country}
              humidity={this.state.humidity}
              pressure={this.state.pressure}
              icon={this.state.icon}
              description={this.state.description}
              error={this.state.error}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default App;
